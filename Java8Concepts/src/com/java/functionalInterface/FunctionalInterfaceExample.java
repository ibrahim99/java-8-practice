package com.java.functionalInterface;

// annotation is not necessary
//@FunctionalInterface
public interface FunctionalInterfaceExample {

	// abstract method
	void hello();

	// we cannot have two abstract methods in an functional interface
//	void hello0(); 

	// default method
	default void hello1() {
		System.out.println("welcome to hello 1");
	}

	// static method
	static void hello2() {
		System.out.println("welcome to hello 2");
	}
}
