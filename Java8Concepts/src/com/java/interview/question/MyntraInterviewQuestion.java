package com.java.interview.question;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MyntraInterviewQuestion {
	/* Find the Nth highest salary of the employee using stream API */

//	public static Map.Entry<String, Integer> getNthHighestSalary(int num, Map<String, Integer> map) {
//		return map.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
//				.collect(Collectors.toList()).get(num - 1);
//	}

	public static Map.Entry<Integer, List<String>> getNthHighestSalary(int num, Map<String, Integer> map) {
		return map.entrySet().stream()
				.collect(Collectors.groupingBy(Map.Entry::getValue,
						Collectors.mapping(Map.Entry::getKey, Collectors.toList())))
				.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByKey()))
				.collect(Collectors.toList()).get(num - 1);
	}

	public static void main(String[] args) {
//		Map<String, Integer> data = new HashMap<>();
//		data.put("Ibrahim", 1000);
//		data.put("Faisal", 1800);
//		data.put("Rizwan", 1100);
//		data.put("Akhlak", 900);
//		data.put("Kaif", 1500);
//		
//
//		// first sort the map.
//		Map.Entry<String, Integer> result = getNthHighestSalary(4, data);
//
//		System.out.println(result);

		Map<String, Integer> data = new HashMap<>();
		data.put("Ibrahim", 1000);
		data.put("Faisal", 1200);
		data.put("Rizwan", 1200);
		data.put("Akhlak", 1000);
		data.put("Kaif", 1500);

		Map.Entry<Integer, List<String>> resultMap = getNthHighestSalary(3, data);
//				data.entrySet().stream()
//				.collect(Collectors.groupingBy(Map.Entry::getValue,
//						Collectors.mapping(Map.Entry::getKey, Collectors.toList())))
//				.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByKey()))
//				.collect(Collectors.toList()).get(1);
		System.out.println(resultMap);

	}
}
