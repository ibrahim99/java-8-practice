package com.java.employees;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmployeesDao {

	public static List<Employees> getAllEmployees() {
		return Stream
				.of(new Employees(101, "Ibrahim", "ibzy@gmail.com", Arrays.asList("901536599", "9818011869")),
						new Employees(101, "Faraz", "f@gmail.com", Arrays.asList("901536566", "6618011869")),
						new Employees(101, "Rizwan", "r@gmail.com", Arrays.asList("9015446599", "7818011869")),
						new Employees(101, "Akhlak", "ak@gmail.com", Arrays.asList("902236599", "8818011869")),
						new Employees(101, "Kaif", "kaif@gmail.com", Arrays.asList("901436599", "4818011869")))
				.collect(Collectors.toList());
	}
}
