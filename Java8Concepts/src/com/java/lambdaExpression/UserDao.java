package com.java.lambdaExpression;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UserDao {

	public List<Users> getAllUsers() {
		List<Users> listOfUSers = new ArrayList<>();
		listOfUSers.add(new Users(1, "Ibrahim", 27, 500000));
		listOfUSers.add(new Users(2, "Rizwan", 28, 600000));
		listOfUSers.add(new Users(3, "Faraz", 28, 400000));
		return listOfUSers;
	}

	public List<Users> getAllGradeUsers() {
		List<Users> listOfUSers = new ArrayList<>();
		listOfUSers.add(new Users(1, "Ibrahim", "A", 27, 500000));
		listOfUSers.add(new Users(2, "Rizwan", "C", 28, 600000));
		listOfUSers.add(new Users(3, "Faraz", "B", 28, 400000));
		listOfUSers.add(new Users(2, "Faisal", "D", 28, 600000));
		listOfUSers.add(new Users(3, "Akhlak", "A", 28, 400000));
		return listOfUSers;
	}

	public List<Users> parallelUsers() {
		List<Users> listOfUSers = new ArrayList<>();
		for (int i = 1; i <= 1000; i++) {
			listOfUSers.add(new Users(i, "User" + i, "A", 10 + i, Integer.valueOf(new Random().nextInt(10 * 100))));
		}
		return listOfUSers;
	}
}
