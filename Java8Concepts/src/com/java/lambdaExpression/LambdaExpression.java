package com.java.lambdaExpression;

interface Calculator {
	// without parameter
//	void switchOn();

	// with parameter
	void sum(int n);
}

public class LambdaExpression {

	public static void main(String[] args) {
		// this is the lambda expression

//		Calculator cal =()->
//			System.out.println("Lambda expression switched onn");		// no args methods.

		Calculator cal = (n) -> {
			if (n > 0) {
				System.out.println("value of N: " + n);
			} else {
				System.out.println("value of N is undefined");
			}

		};

		cal.sum(0);
//		cal.switchOn();
	}

}
