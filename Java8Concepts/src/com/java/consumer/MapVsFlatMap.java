package com.java.consumer;

import java.util.List;
import java.util.stream.Collectors;

import com.java.employees.Employees;
import com.java.employees.EmployeesDao;

public class MapVsFlatMap {

	public static void main(String[] args) {
		new EmployeesDao();
		List<Employees> employees = EmployeesDao.getAllEmployees();

		// map() method example (One-To-One)
		// List<Employees> converts into List<String> this is called Data Transformation
		// mapping: emp -> emp.getEmail()
		List<String> emailsList = employees.stream().map(emp -> emp.getEmail()).collect(Collectors.toList());
		System.out.println(emailsList);

		// flatMap() method example
		// emp: emp -> emp.getPhone() (One-To-Many Mapping)
		List<String> phoneList = employees.stream().flatMap(emp -> emp.getPhone().stream())
				.collect(Collectors.toList());
		System.out.println(phoneList);

	}

}
