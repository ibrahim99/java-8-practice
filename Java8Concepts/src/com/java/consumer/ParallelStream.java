package com.java.consumer;

import java.util.List;

import com.java.lambdaExpression.UserDao;
import com.java.lambdaExpression.Users;

public class ParallelStream {
	/*
	 * Parallel stream is use to consume multiple cores of the CPU it is fast as
	 * compare to sequential stream. we don't have the control on parallel stream
	 * processing.
	 */
	public static void main(String[] args) {

		long start = 0;
		long end = 0;

//		start = System.currentTimeMillis();
//		IntStream.range(1, 100).forEach(System.out::println);
//		end = System.currentTimeMillis();
//		System.out.println("sequntial stream execution time: " + (end - start));
//
//		System.out.println("=================================================");
//
//		start = System.currentTimeMillis();
//		IntStream.range(1, 100).parallel().forEach(System.out::println);
//		end = System.currentTimeMillis();
//		System.out.println("parallel stream execution time: " + (end - start));

		List<Users> users = new UserDao().parallelUsers();

		start = System.currentTimeMillis();
		double seqSalary = users.stream().map(u -> u.getSalary()).mapToDouble(i -> i).average().getAsDouble();
		end = System.currentTimeMillis();
		System.out.println("Normal stream execution time: " + (end - start) + " : salary: " + seqSalary);

		System.out.println("=================================================");

		start = System.currentTimeMillis();
		double parallelSalary = users.parallelStream().map(u -> u.getSalary()).mapToDouble(i -> i).average()
				.getAsDouble();
		end = System.currentTimeMillis();
		System.out.println("parallel stream execution time: " + (end - start) + " : salary: " + parallelSalary);
	}
}
