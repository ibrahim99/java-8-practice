package com.java.consumer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.java.lambdaExpression.UserDao;
import com.java.lambdaExpression.Users;

public class SortByStream {

	public static void main(String[] args) {

		List<Integer> nums = new ArrayList<>();
		nums.add(1);
		nums.add(2);
		nums.add(3);
		nums.add(4);
		List<Users> userslList = new UserDao().getAllUsers();
		userslList.stream().sorted(Comparator.comparing(Users::getName)).forEach(System.out::println);
	}
}
