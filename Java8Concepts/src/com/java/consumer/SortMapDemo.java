package com.java.consumer;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.java.lambdaExpression.UserDao;
import com.java.lambdaExpression.Users;

public class SortMapDemo {

	public static void main(String[] args) {

		Map<String, Integer> map = new HashMap<>();
		map.put("eight", 8);
		map.put("four", 4);
		map.put("one", 1);
		map.put("nine", 9);

		// STREAM API
//		map.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(System.out::println);

		System.out.println("User data starts");
		List<Users> userList = new UserDao().getAllUsers();
		Map<Users, Integer> userMap = new HashMap<>();
//				new TreeMap<>((o1, o2) -> o1.getSalary() - o2.getSalary());

		for (Users users : userList) {
			userMap.put(users, users.getId());
		}

		userMap.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.comparing(Users::getAge)))
				.forEach(System.out::println);
		System.out.println("==================================================");
		System.out.println(userMap);

//
//		System.out.println(userMap);

		// TRADITIONAL APPROACH
//		List<Entry<String, Integer>> entries = new ArrayList<>(map.entrySet());
//		Collections.sort(entries, (o1, o2) -> o1.getKey().compareTo(o2.getKey()));
//		for (Entry<String, Integer> entry : entries) {
//			System.out.println(entry.getKey() + " : " + entry.getValue());
//		}
	}
}
