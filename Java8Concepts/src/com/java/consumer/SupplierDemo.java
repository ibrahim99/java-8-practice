package com.java.consumer;

import java.util.ArrayList;
import java.util.List;

import com.java.lambdaExpression.Users;

public class SupplierDemo {

	public static void main(String[] args) {

		List<Users> users = new ArrayList<>();
//				new UserDao().getAllUsers();
		System.out.println(users.stream().findAny().orElseGet(() -> new Users(4, "akhlak", 20)));
	}
//		Supplier<String> supplier = () -> {
//			return "hello folks";
//		};
//		System.out.println(supplier.get());
//	}

	// BELOW WAS THE BASIC EXAMPLE FOR SUPPLIER INTERFACE.
//implements Supplier<String> {
//	@Override
//	public String get() {
//
//		return "hello Welcome to Supplier";
//	}
//
//	public static void main(String[] args) {
//		SupplierDemo supplierDemo = new SupplierDemo();
//		System.out.println(supplierDemo.get());
//	}
}
