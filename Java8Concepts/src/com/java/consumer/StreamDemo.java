package com.java.consumer;

import java.util.List;
import java.util.stream.Collectors;

import com.java.lambdaExpression.UserDao;
import com.java.lambdaExpression.Users;

public class StreamDemo {

	public static List<Users> filterByUserSalary() {

		List<Users> listOfUsers = new UserDao().getAllUsers().stream().filter(u -> u.getSalary() > 450000)
				.collect(Collectors.toList());

		return listOfUsers;
	}

	public static void main(String[] args) {
		System.out.println(filterByUserSalary());
	}
}
