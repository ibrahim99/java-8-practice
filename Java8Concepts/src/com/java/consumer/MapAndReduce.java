package com.java.consumer;

import java.util.Arrays;
import java.util.List;

import com.java.lambdaExpression.UserDao;
import com.java.lambdaExpression.Users;

public class MapAndReduce {

	public static void main(String[] args) {
		List<Integer> nums = Arrays.asList(1, 2, 3, 9, 4, 5);

		// TRADITIONAL APPROACH
		int sum = 0;
		for (Integer n : nums) {
			sum = sum + n;
		}
		System.out.println(sum);

		// map
		int sumStream = nums.stream().mapToInt(i -> i).sum();
		System.out.println(sumStream);

		// reduce
		int reduceSum = nums.stream().reduce(0, (a, b) -> (a + b));
		System.out.println(reduceSum);

		int sumMethodRef = nums.stream().reduce(0, Integer::sum);
		System.out.println(sumMethodRef);

		int maxValue = nums.stream().reduce(0, (Integer::max));
		System.out.println(maxValue);

		List<String> longestStrings = Arrays.asList("springBoot", "java", "reactjs", "hibernate");
		String word = longestStrings.stream().reduce((w1, w2) -> w1.length() > w2.length() ? w1 : w2).get();
		System.out.println(word);

		// Evaluate average salary of grade A user.
		List<Users> users = new UserDao().getAllGradeUsers();
		double avgsalary = users.stream().filter(u -> u.getGrade().equals("A")).map(u -> u.getSalary())
				.mapToDouble(i -> i).average().getAsDouble();

		System.out.println(avgsalary);

		double sumSalary = users.stream().filter(u -> u.getGrade().equals("A")).map(u -> u.getSalary())
				.mapToDouble(i -> i).sum();
		System.out.println(sumSalary);

	}
}
