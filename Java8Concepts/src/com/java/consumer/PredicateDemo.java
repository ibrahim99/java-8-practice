package com.java.consumer;

import java.util.List;

import com.java.lambdaExpression.UserDao;
import com.java.lambdaExpression.Users;

public class PredicateDemo {

	public static void main(String[] args) {
//		Predicate<Integer> predicate = (t) -> t % 2 == 0;
//		System.out.println(predicate.test(9));

//		List<Integer> nums = Arrays.asList(1, 2, 3, 4, 5);
//		nums.stream().filter(predicate).forEach(t -> System.out.println("print event: " + t));

		List<Users> usersList = new UserDao().getAllUsers();
		usersList.stream().filter(t -> t.getName().equals("ibrahim"))
				.forEach(t -> System.out.println("users: index: " + usersList.indexOf(t) + " and value: " + t));
	}
//implements Predicate<Integer> {

	// BASIC UNDERSTANDING CODE FOR PREDICATES.
//	@Override
//	public boolean test(Integer t) {
//		if (t % 2 == 0) {
//			return true;
//		} else {
//			return false;
//		}
//	}
//
//	public static void main(String[] args) {
//		PredicateDemo predicateDemo = new PredicateDemo();
//		System.out.println(predicateDemo.test(4));
//	}
}
