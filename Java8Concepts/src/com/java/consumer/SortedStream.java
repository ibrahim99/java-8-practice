package com.java.consumer;

import java.util.Comparator;
import java.util.List;

import com.java.lambdaExpression.UserDao;
import com.java.lambdaExpression.Users;

public class SortedStream {

	public static void main(String[] args) {
		List<Users> users = new UserDao().getAllUsers();
		// through collections
//		Collections.sort(users, (o1, o2) -> o2.getName().compareTo(o1.getName()));

		// through stream API
		users.stream().sorted(Comparator.comparing(Users::getName)).forEach(System.out::println);

//		System.out.println(users);

	}
}
