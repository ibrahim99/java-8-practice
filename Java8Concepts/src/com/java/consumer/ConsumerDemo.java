package com.java.consumer;

import java.util.List;

import com.java.lambdaExpression.UserDao;
import com.java.lambdaExpression.Users;

public class ConsumerDemo {

	public static void main(String[] args) {
		// THIS WAS THE BASIC EXAMPLE OF CONSUMER FOR UNDERSTANDING
//		Consumer<Integer> consumer = t -> System.out.println("value of consumer: " + t);
//		consumer.accept(10);

		List<Users> usersList = new UserDao().getAllUsers();
		usersList.stream()
				.forEach(t -> System.out.println("users: index: " + usersList.indexOf(t) + "and value: " + t));
	}

}
